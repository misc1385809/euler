#ifndef EULER_PROBLEM_PROBLEM
#define EULER_PROBLEM_PROBLEM

#include <string>

namespace euler
{
    /// @brief An interface for a problem to solve.
    class problem
    {
    public:
        /// @brief Destruct a problem object.
        virtual ~problem() = default;

        /// @brief Solve the problem.
        virtual std::string solve() = 0;
    };
}

#endif
