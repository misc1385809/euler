#ifndef EULER_PROBLEM_PROBLEM3
#define EULER_PROBLEM_PROBLEM3

#include "problem.hxx"
#include "../utility/is_prime.hxx"

namespace euler {
    /// @brief Problem 3: Largest prime factor.
    /// @details The prime factors of 13195 are 5, 7, 13 and 29.
    /// What is the largest prime factor of the number 600851475143?
    class problem3 : public problem {
    public:
        std::string solve() override {
            long target = 600'851'475'143;
            long largest_prime = -1;

            for (long i = 2; i <= target; ++i) {
                if (target % i == 0) {
                    if (is_prime(i) && i > largest_prime) {
                        largest_prime = i;
                    }
                    target = target / i;
                    i = 1;
                }
            }

            return std::to_string(largest_prime);
        }
    };
}

#endif
