#ifndef EULER_PROBLEM_PROBLEM4
#define EULER_PROBLEM_PROBLEM4

#include "problem.hxx"
#include "../utility/is_palindrome.hxx"

namespace euler
{
    /// @brief Problem 4: Largest palindrome product
    /// @details A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
    /// Find the largest palindrome made from the product of two 3-digit numbers.
    class problem4 : public problem {
    public:
        std::string solve() override {
            int largest_palindrome = -1;

            for (int i = 100; i < 1000; ++i) {
                for (int j = i; j < 1000; ++j) {
                    int product = i * j;
                    auto str = std::to_string(product);
                    if (is_palindrome(str) && product > largest_palindrome) {
                        largest_palindrome = product;
                    }
                }
            }

            return std::to_string(largest_palindrome);
        }
    };
}

#endif
