#ifndef EULER_PROBLEM_PROBLEM1
#define EULER_PROBLEM_PROBLEM1

#include <string>
#include "problem.hxx"

namespace euler {
    /// @brief Problem 1: Multiples of 3 or 5
    /// @details If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
    /// The sum of these multiples is 23. Find the sum of all the multiples of 3 or 5 below 1000.
    class problem1 : public problem {
    public:
        std::string solve() override {
            int sum = 0;
            for (int i = 3; i < 1000; ++i) {
                if ((i % 3 == 0) || (i % 5 == 0)) {
                    sum += i;
                }
            }
            return std::to_string(sum);
        }
    };
}

#endif
