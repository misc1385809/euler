#ifndef EULER_UTILITY_STOPWATCH
#define EULER_UTILITY_STOPWATCH

#include <chrono>
#include <optional>

namespace euler {
    /// @brief A stopwatch to measure time.
    /// @tparam Duration The duration type.
    template <typename Duration = std::chrono::milliseconds>
    class stopwatch {
    public:
        /// @brief The duration type.
        using duration_type = Duration;

        /// @brief Get the total elapsed time.
        duration_type get_elapsed() const {
            return _elapsed;
        }

        /// @brief Start the stopwatch.
        void start() {
            if (!_start) {
                _start = std::chrono::high_resolution_clock::now();
            }
        }

        /// @brief Stop the stopwatch.
        void stop() {
            if (_start) {
                auto now = std::chrono::high_resolution_clock::now();
                _elapsed += std::chrono::duration_cast<duration_type>(now - _start.value());
                _start = std::nullopt;
            }
        }

        /// @brief Reset the stopwatch.
        void reset() {
            _start = std::nullopt;
            _elapsed = duration_type();
        }

    private:
        /// @brief The start time point.
        std::optional<std::chrono::time_point<std::chrono::high_resolution_clock>> _start;

        /// @brief The total time elapsed.
        duration_type _elapsed;
    };
}

#endif
