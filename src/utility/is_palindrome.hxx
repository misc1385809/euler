#ifndef EULER_UTILITY_IS_PALINDROME
#define EULER_UTILITY_IS_PALINDROME

#include <string_view>

namespace euler {
    /// @brief Determines if a string is a palindrome.
    /// @param str The string to check.
    /// @return True if a palindrome, otherwise false.
    bool is_palindrome(std::string_view str) {
        auto size = str.size();
        auto pivot = size / 2;
        for (int i = 0; i < pivot; ++i) {
            if (str[i] != str[size - i - 1]) {
                return false;
            }
        }
        return true;
    }
}

#endif
