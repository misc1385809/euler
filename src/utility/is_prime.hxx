#ifndef EULER_UTILITY_IS_PRIME
#define EULER_UTILITY_IS_PRIME

namespace euler {
    /// @brief Determines if a number is prime.
    constexpr bool is_prime(long number) noexcept {
        for (long i = 2; i < number; ++i) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}

#endif
