#include <iostream>
#include <map>
#include <memory>
#include <string_view>
#include "problem/all_problems.hxx"
#include "utility/stopwatch.hxx"

static std::map<std::string_view, std::shared_ptr<euler::problem>> problems {
    {"1", std::make_shared<euler::problem1>()},
    {"2", std::make_shared<euler::problem2>()},
    {"3", std::make_shared<euler::problem3>()},
    {"4", std::make_shared<euler::problem4>()}
};

int main(int argc, char** argv) {
    if (argc < 2) {
        std::cerr << "usage: " << argv[0] << " id\n";
        return 1;
    }

    std::string_view id(argv[1]);
    if (problems.contains(id)) {
        euler::stopwatch sw;
        sw.start();
        auto sln = problems[id]->solve();
        sw.stop();

        auto duration{sw.get_elapsed()};
        std::cout << "problem " << id << " solution: " << sln  << " (" << duration << ")" << std::endl;
    }

    return 0;
}
